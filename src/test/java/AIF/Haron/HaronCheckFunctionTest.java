package AIF.Haron;

//incomplete ...

import AIF.AIFUtil;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class HaronCheckFunctionTest {

    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }

    @BeforeClass
    public static void beforeAll(){
        aifUtil = new AIFUtil();//init  aerial vehicles and missions.
    }




    //קבוצות שקילות
    @Test//78
    public void testCheckHermesEquilibriumGroupsInGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),78);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 78 shouldn't be reset to 0 after Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 78);
    }


    @Test//156
    public void testCheckHermesEquilibriumGroupsAboveGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),156);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 156 should  0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }









    //ערכי גבול

    @Test
    public void testCheckLimitsFighterJetsLowerBoundMiddle(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundLeft(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),-1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 shouldn't be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }



    @Test
    public void testCheckLimitsFighterJetsLowerBoundRight(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 1 should be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 1);
    }


    @Test
    public void testCheckLimitsFighterJetsUpperBoundRight(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),151);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 151 should be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }







}
